#ifndef DOZEN_PLUS_H
#define DOZEN_PLUS_H

#include <cstdint>
#include <memory>
#include <vector>

namespace crypto
{
    using DataBlock = std::array<uint8_t, 64>;
    using DataChain = std::vector<uint8_t>;

    class DozenPlus
    {
    public:
        explicit DozenPlus();
        virtual ~DozenPlus();

        DataChain ecbEncrypt(const DataChain &data, const DataBlock &key);
        DataChain ecbDecrypt(const DataChain &data, const DataBlock &key);

        DataChain parallelEcbEncrypt(const DataChain &data, const DataBlock &key);
        DataChain parallelEcbDecrypt(const DataChain &data, const DataBlock &key);

    private:
        struct Impl;
        const std::unique_ptr<Impl> d;
    };
}  // namespace crypto

#endif // DOZEN_PLUS_H
