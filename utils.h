#ifndef UTILS_H
#define UTILS_H

#include <iostream>

namespace utils
{
    template<typename T>
    void printHexDataContainer(const T &data, const std::string &header = {})
    {
        if (!header.empty()) std::cout << header << std::endl;

        for (auto byte: data)
        {
            std::cout << std::hex << static_cast<int>(byte) << " ";
        }
        std::cout << std::endl;

        if (!header.empty()) std::cout << std::endl;
    }
}

#endif // UTILS_H
