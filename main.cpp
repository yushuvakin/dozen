#include <iostream>
#include <algorithm>
#include <chrono>
#include <ctime>

#include "dozen_plus.h"
#include "utils.h"

using namespace std::chrono;

int main()
{
    const auto begin_time = high_resolution_clock::now();
    std::srand(unsigned(std::time(nullptr)));

    // 1048576 - 1 MB
    // 67108864 - 64 MB
    // 1073741824 - 1 GB
    crypto::DataChain data(67108864);
    std::generate(data.begin(), data.end(), std::rand);
//    utils::printHexDataContainer(data, "Source data");

    crypto::DataBlock key{};
    std::generate(key.begin(), key.end(), std::rand);
//    utils::printHexDataContainer(key, "Key");

    crypto::DozenPlus dozen;

//    auto encryptedData = dozen.ecbEncrypt(data, key);
    auto encryptedData = dozen.parallelEcbEncrypt(data, key);

//    utils::printHexDataContainer(encryptedData, "Encrypted data");
    std::cout << "Encryption elapsed time: "
              << duration_cast<milliseconds>(high_resolution_clock::now() - begin_time).count() / 1000.0
              << std::endl;

//    auto decryptedData = dozen.ecbDecrypt(encryptedData, key);
    auto decryptedData = dozen.parallelEcbDecrypt(encryptedData, key);

//    utils::printHexDataContainer(decryptedData, "Decrypted data");
    std::cout << "Decryption elapsed time: "
              << duration_cast<milliseconds>(high_resolution_clock::now() - begin_time).count() / 1000.0
              << std::endl;

    std::cout << "Is equal: " << std::boolalpha
              << std::equal(data.begin(), data.end(), decryptedData.begin())
              << std::endl;

    return 0;
}
