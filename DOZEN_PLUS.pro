TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

#CONFIG += optimize_full

#QMAKE_CXXFLAGS_RELEASE -= -O2
#QMAKE_CXXFLAGS_RELEASE += -O3

QMAKE_CXXFLAGS += -fopenmp

LIBS += -pthread
LIBS += -fopenmp

SOURCES += \
    main.cpp \
    dozen_plus.cpp

HEADERS += \
    dozen_plus.h \
    utils.h \
    precomputed_tables.h
