#include "dozen_plus.h"
#include "precomputed_tables.h"

#include <omp.h>

#include <functional>
#include <algorithm>
#include <map>
#include <thread>
#include <future>

#include <iostream>


namespace
{
    const std::array<uint8_t, 256> sBox =
    {
        0xFC, 0xEE, 0xDD, 0x11, 0xCF, 0x6E, 0x31, 0x16,
        0xFB, 0xC4, 0xFA, 0xDA, 0x23, 0xC5, 0x04, 0x4D,
        0xE9, 0x77, 0xF0, 0xDB, 0x93, 0x2E, 0x99, 0xBA,
        0x17, 0x36, 0xF1, 0xBB, 0x14, 0xCD, 0x5F, 0xC1,
        0xF9, 0x18, 0x65, 0x5A, 0xE2, 0x5C, 0xEF, 0x21,
        0x81, 0x1C, 0x3C, 0x42, 0x8B, 0x01, 0x8E, 0x4F,
        0x05, 0x84, 0x02, 0xAE, 0xE3, 0x6A, 0x8F, 0xA0,
        0x06, 0x0B, 0xED, 0x98, 0x7F, 0xD4, 0xD3, 0x1F,
        0xEB, 0x34, 0x2C, 0x51, 0xEA, 0xC8, 0x48, 0xAB,
        0xF2, 0x2A, 0x68, 0xA2, 0xFD, 0x3A, 0xCE, 0xCC,
        0xB5, 0x70, 0x0E, 0x56, 0x08, 0x0C, 0x76, 0x12,
        0xBF, 0x72, 0x13, 0x47, 0x9C, 0xB7, 0x5D, 0x87,
        0x15, 0xA1, 0x96, 0x29, 0x10, 0x7B, 0x9A, 0xC7,
        0xF3, 0x91, 0x78, 0x6F, 0x9D, 0x9E, 0xB2, 0xB1,
        0x32, 0x75, 0x19, 0x3D, 0xFF, 0x35, 0x8A, 0x7E,
        0x6D, 0x54, 0xC6, 0x80, 0xC3, 0xBD, 0x0D, 0x57,
        0xDF, 0xF5, 0x24, 0xA9, 0x3E, 0xA8, 0x43, 0xC9,
        0xD7, 0x79, 0xD6, 0xF6, 0x7C, 0x22, 0xB9, 0x03,
        0xE0, 0x0F, 0xEC, 0xDE, 0x7A, 0x94, 0xB0, 0xBC,
        0xDC, 0xE8, 0x28, 0x50, 0x4E, 0x33, 0x0A, 0x4A,
        0xA7, 0x97, 0x60, 0x73, 0x1E, 0x00, 0x62, 0x44,
        0x1A, 0xB8, 0x38, 0x82, 0x64, 0x9F, 0x26, 0x41,
        0xAD, 0x45, 0x46, 0x92, 0x27, 0x5E, 0x55, 0x2F,
        0x8C, 0xA3, 0xA5, 0x7D, 0x69, 0xD5, 0x95, 0x3B,
        0x07, 0x58, 0xB3, 0x40, 0x86, 0xAC, 0x1D, 0xF7,
        0x30, 0x37, 0x6B, 0xE4, 0x88, 0xD9, 0xE7, 0x89,
        0xE1, 0x1B, 0x83, 0x49, 0x4C, 0x3F, 0xF8, 0xFE,
        0x8D, 0x53, 0xAA, 0x90, 0xCA, 0xD8, 0x85, 0x61,
        0x20, 0x71, 0x67, 0xA4, 0x2D, 0x2B, 0x09, 0x5B,
        0xCB, 0x9B, 0x25, 0xD0, 0xBE, 0xE5, 0x6C, 0x52,
        0x59, 0xA6, 0x74, 0xD2, 0xE6, 0xF4, 0xB4, 0xC0,
        0xD1, 0x66, 0xAF, 0xC2, 0x39, 0x4B, 0x63, 0xB6,
    };

    const std::array<uint8_t, 256> rsBox =
    {
        0xA5, 0x2D, 0x32, 0x8F, 0x0E, 0x30, 0x38, 0xC0,
        0x54, 0xE6, 0x9E, 0x39, 0x55, 0x7E, 0x52, 0x91,
        0x64, 0x03, 0x57, 0x5A, 0x1C, 0x60, 0x07, 0x18,
        0x21, 0x72, 0xA8, 0xD1, 0x29, 0xC6, 0xA4, 0x3F,
        0xE0, 0x27, 0x8D, 0x0C, 0x82, 0xEA, 0xAE, 0xB4,
        0x9A, 0x63, 0x49, 0xE5, 0x42, 0xE4, 0x15, 0xB7,
        0xC8, 0x06, 0x70, 0x9D, 0x41, 0x75, 0x19, 0xC9,
        0xAA, 0xFC, 0x4D, 0xBF, 0x2A, 0x73, 0x84, 0xD5,
        0xC3, 0xAF, 0x2B, 0x86, 0xA7, 0xB1, 0xB2, 0x5B,
        0x46, 0xD3, 0x9F, 0xFD, 0xD4, 0x0F, 0x9C, 0x2F,
        0x9B, 0x43, 0xEF, 0xD9, 0x79, 0xB6, 0x53, 0x7F,
        0xC1, 0xF0, 0x23, 0xE7, 0x25, 0x5E, 0xB5, 0x1E,
        0xA2, 0xDF, 0xA6, 0xFE, 0xAC, 0x22, 0xF9, 0xE2,
        0x4A, 0xBC, 0x35, 0xCA, 0xEE, 0x78, 0x05, 0x6B,
        0x51, 0xE1, 0x59, 0xA3, 0xF2, 0x71, 0x56, 0x11,
        0x6A, 0x89, 0x94, 0x65, 0x8C, 0xBB, 0x77, 0x3C,
        0x7B, 0x28, 0xAB, 0xD2, 0x31, 0xDE, 0xC4, 0x5F,
        0xCC, 0xCF, 0x76, 0x2C, 0xB8, 0xD8, 0x2E, 0x36,
        0xDB, 0x69, 0xB3, 0x14, 0x95, 0xBE, 0x62, 0xA1,
        0x3B, 0x16, 0x66, 0xE9, 0x5C, 0x6C, 0x6D, 0xAD,
        0x37, 0x61, 0x4B, 0xB9, 0xE3, 0xBA, 0xF1, 0xA0,
        0x85, 0x83, 0xDA, 0x47, 0xC5, 0xB0, 0x33, 0xFA,
        0x96, 0x6F, 0x6E, 0xC2, 0xF6, 0x50, 0xFF, 0x5D,
        0xA9, 0x8E, 0x17, 0x1B, 0x97, 0x7D, 0xEC, 0x58,
        0xF7, 0x1F, 0xFB, 0x7C, 0x09, 0x0D, 0x7A, 0x67,
        0x45, 0x87, 0xDC, 0xE8, 0x4F, 0x1D, 0x4E, 0x04,
        0xEB, 0xF8, 0xF3, 0x3E, 0x3D, 0xBD, 0x8A, 0x88,
        0xDD, 0xCD, 0x0B, 0x13, 0x98, 0x02, 0x93, 0x80,
        0x90, 0xD0, 0x24, 0x34, 0xCB, 0xED, 0xF4, 0xCE,
        0x99, 0x10, 0x44, 0x40, 0x92, 0x3A, 0x01, 0x26,
        0x12, 0x1A, 0x48, 0x68, 0xF5, 0x81, 0x8B, 0xC7,
        0xD6, 0x20, 0x0A, 0x08, 0x00, 0x4C, 0xD7, 0x74
    };

    const std::array<uint8_t, 16> lbox =
    {
        0x94, 0x20, 0x85, 0x10, 0xC2, 0xC0, 0x01, 0xFB,
        0x01, 0xC0, 0xC2, 0x10, 0x85, 0x20, 0x94, 0x01
    };

    const crypto::DataBlock xConstantKey =
    {
        0x0C, 0x02, 0x03, 0x08, 0x0C, 0x02, 0x03, 0x08, 0x0C, 0x02, 0x03, 0x08, 0x0C, 0x02, 0x03, 0x08,
        0x0C, 0x02, 0x03, 0x08, 0x0C, 0x02, 0x03, 0x08, 0x0C, 0x02, 0x03, 0x08, 0x0C, 0x02, 0x03, 0x08,
        0x0C, 0x02, 0x03, 0x08, 0x0C, 0x02, 0x03, 0x08, 0x0C, 0x02, 0x03, 0x08, 0x0C, 0x02, 0x03, 0x08,
        0x0C, 0x02, 0x03, 0x08, 0x0C, 0x02, 0x03, 0x08, 0x0C, 0x02, 0x03, 0x08, 0x0C, 0x02, 0x03, 0x08
    };

    const crypto::DataBlock yConstantKey =
    {
        0x02, 0x05, 0x1A, 0x05, 0x02, 0x05, 0x1A, 0x05, 0x02, 0x05, 0x1A, 0x05, 0x02, 0x05, 0x1A, 0x05,
        0x02, 0x05, 0x1A, 0x05, 0x02, 0x05, 0x1A, 0x05, 0x02, 0x05, 0x1A, 0x05, 0x02, 0x05, 0x1A, 0x05,
        0x02, 0x05, 0x1A, 0x05, 0x02, 0x05, 0x1A, 0x05, 0x02, 0x05, 0x1A, 0x05, 0x02, 0x05, 0x1A, 0x05,
        0x02, 0x05, 0x1A, 0x05, 0x02, 0x05, 0x1A, 0x05, 0x02, 0x05, 0x1A, 0x05, 0x02, 0x05, 0x1A, 0x05,
    };

    const crypto::DataBlock zConstantKey =
    {
        0x16, 0x09, 0x19, 0x0A, 0x16, 0x09, 0x19, 0x0A, 0x16, 0x09, 0x19, 0x0A, 0x16, 0x09, 0x19, 0x0A,
        0x16, 0x09, 0x19, 0x0A, 0x16, 0x09, 0x19, 0x0A, 0x16, 0x09, 0x19, 0x0A, 0x16, 0x09, 0x19, 0x0A,
        0x16, 0x09, 0x19, 0x0A, 0x16, 0x09, 0x19, 0x0A, 0x16, 0x09, 0x19, 0x0A, 0x16, 0x09, 0x19, 0x0A,
        0x16, 0x09, 0x19, 0x0A, 0x16, 0x09, 0x19, 0x0A, 0x16, 0x09, 0x19, 0x0A, 0x16, 0x09, 0x19, 0x0A
    };

    const uint8_t blockSize = 64;
    const uint8_t layerSize = 16;
    const uint8_t layerWidth = 4;
    const uint8_t layersCount = 4;
}  // namespace


using namespace crypto;

using LayerBlock = std::array<uint8_t, 16>;
using RoundKeyArray = std::array<DataBlock, 3>;


struct DozenPlus::Impl
{
    enum class LayerType
    {
        X,
        Y,
        Z
    };

    // index = 16 * x + 4 * y + z;
    std::map<LayerType, std::function<uint8_t(uint8_t, uint8_t, uint8_t)>> layerIndexesFunctors =
    {
        {LayerType::X, [](uint8_t layerNumber, uint8_t firstSide, uint8_t secondSide)
                         { return 16 * layerNumber + 4 * firstSide + secondSide; }},

        {LayerType::Y, [](uint8_t layerNumber, uint8_t firstSide, uint8_t secondSide)
                         { return 16 * firstSide + 4 * layerNumber + secondSide; }},

        {LayerType::Z, [](uint8_t layerNumber, uint8_t firstSide, uint8_t secondSide)
                         { return 16 * firstSide + 4 * secondSide + layerNumber; }},
    };

    RoundKeyArray keyExpansion(DataBlock key)
    {
        RoundKeyArray keyArray;

        for (auto &roundKey : keyArray)
        {
            this->mixLayersWithSameType(key, LayerType::X, ::xConstantKey);
            this->mixLayersWithSameType(key, LayerType::Y, ::yConstantKey);
            this->mixLayersWithSameType(key, LayerType::X, ::zConstantKey);

            roundKey = key;
        }

        return keyArray;
    }

    LayerBlock extractLayerFromData(const DataBlock &dataBlock,
                                    LayerType layerType, uint8_t layerNumber)
    {
        LayerBlock layer;
        auto indexesFunctor = layerIndexesFunctors[layerType];
        auto layerIterator = layer.begin();

        for (uint8_t firstSide = 0; firstSide < ::layerWidth; ++firstSide)
        {
            for (uint8_t secondSide = 0; secondSide < ::layerWidth; ++secondSide)
            {
                *layerIterator = dataBlock.at(indexesFunctor(layerNumber, firstSide, secondSide));
                ++layerIterator;
            }
        }

        return layer;
    }

    void insertLayerToData(DataBlock &dataBlock, const LayerBlock &layer,
                           LayerType layerType, uint8_t layerNumber)
    {
        auto indexesFunctor = layerIndexesFunctors[layerType];
        auto layerIterator = layer.cbegin();

        for (uint8_t firstSide = 0; firstSide < ::layerWidth; ++firstSide)
        {
            for (uint8_t secondSide = 0; secondSide < ::layerWidth; ++secondSide)
            {
                dataBlock[indexesFunctor(layerNumber, firstSide, secondSide)] = *layerIterator;
                ++layerIterator;
            }
        }
    }

    void addRoundSubKeyToDataBlock(DataBlock &data, const DataBlock &key)
    {
        auto keyIterator = key.cbegin();

        for (auto &byte: data)
        {
            byte ^= *keyIterator;
            ++keyIterator;
        }
    }

    void mixLayersWithSameType(DataBlock &data, LayerType layerType, const DataBlock &key)
    {
        using namespace std::placeholders;
        this->mixLayersWithSameTypeImpl(data, layerType, key,
                                        std::bind(&Impl::mixLayer, this, _1, _2));
    }

    void invMixLayersWithSameType(DataBlock &data, LayerType layerType, const DataBlock &key)
    {
        using namespace std::placeholders;
        this->mixLayersWithSameTypeImpl(data, layerType, key,
                                        std::bind(&Impl::invMixLayer, this, _1, _2));
    }

    template<typename T>
    void mixLayersWithSameTypeImpl(DataBlock &data, LayerType layerType,
                                   const DataBlock &key, T mixFunctor)
    {
        auto keyIterator = key.cbegin();

        for (uint8_t layerNumber = 0; layerNumber < ::layersCount; ++layerNumber)
        {
            auto layerBlock = this->extractLayerFromData(data, layerType, layerNumber);
            mixFunctor(layerBlock, keyIterator);
            this->insertLayerToData(data, layerBlock, layerType, layerNumber);
        }
    }

    void parallelMixLayersWithSameType(DataBlock &data, LayerType layerType, const DataBlock &key)
    {
        using namespace std::placeholders;
        this->parallelMixLayersWithSameTypeImpl(data, layerType, key,
                                                std::bind(&Impl::mixLayer, this, _1, _2));
    }

    void parallelInvMixLayersWithSameType(DataBlock &data, LayerType layerType, const DataBlock &key)
    {
        using namespace std::placeholders;
        this->parallelMixLayersWithSameTypeImpl(data, layerType, key,
                                                std::bind(&Impl::invMixLayer, this, _1, _2));
    }

    // so slow...
    template<typename T>
    void threadedMixLayersWithSameTypeImpl(DataBlock &data, LayerType layerType,
                                           const DataBlock &key, T mixFunctor)
    {
        std::array<LayerBlock, ::layersCount> layerArray;
        std::array<std::thread, ::layersCount> threadArray;

        for (uint8_t layerNumber = 0; layerNumber < ::layersCount; ++layerNumber)
        {
            threadArray[layerNumber] = std::thread([&, layerNumber]()
            {
                layerArray[layerNumber] = this->extractLayerFromData(data, layerType, layerNumber);
                auto keyIterator = key.cbegin() + layerNumber * ::layerSize;
                mixFunctor(layerArray[layerNumber], keyIterator);
                this->insertLayerToData(data, layerArray.at(layerNumber), layerType, layerNumber);
            });
        }

        for (uint8_t layerNumber = 0; layerNumber < ::layersCount; ++layerNumber)
        {
            threadArray[layerNumber].join();
        }
    }

    template<typename T>
    void parallelMixLayersWithSameTypeImpl(DataBlock &data, LayerType layerType,
                                           const DataBlock &key, T mixFunctor)
    {
#pragma omp for
        for (uint8_t layerNumber = 0; layerNumber < ::layersCount; ++layerNumber)
        {
            auto layerBlock = this->extractLayerFromData(data, layerType, layerNumber);
            auto keyIterator = key.cbegin() + layerNumber * ::layerSize;
            mixFunctor(layerBlock, keyIterator);
            this->insertLayerToData(data, layerBlock, layerType, layerNumber);
        }
    }

    void mixLayer(LayerBlock &layerBlock, DataBlock::const_iterator &keyIterator)
    {
        this->subBytes(layerBlock);
        this->mixState(layerBlock);
        this->addRoundSubKeyToLayer(layerBlock, keyIterator);
    }

    void invMixLayer(LayerBlock &layerBlock, DataBlock::const_iterator &keyIterator)
    {
        this->addRoundSubKeyToLayer(layerBlock, keyIterator);
        this->invMixState(layerBlock);
        this->invSubBytes(layerBlock);
    }

    void subBytes(LayerBlock &layerBlock)
    {
        for (auto &byte: layerBlock)
        {
            byte = ::sBox.at(byte);
        }
    }

    void invSubBytes(LayerBlock &layerBlock)
    {
        for (auto &byte: layerBlock)
        {
            byte = ::rsBox.at(byte);
        }
    }

    void mixState(LayerBlock &layerBlock)
    {
        this->lTransformation(layerBlock);
    }

    void invMixState(LayerBlock &layerBlock)
    {
        this->invLTransformation(layerBlock);
    }

    void addRoundSubKeyToLayer(LayerBlock &layerBlock, DataBlock::const_iterator &keyIterator)
    {
        for (auto &byte: layerBlock)
        {
            byte ^= *keyIterator;
            ++keyIterator;
        }
    }

    void lTransformation(LayerBlock &layerBlock)
    {
        for (auto &byte: layerBlock)
        {
            uint8_t mixed_byte = *layerBlock.rbegin();

            for (int i = ::layerSize - 2; i >= 0; --i)
            {
                layerBlock[i + 1] = layerBlock.at(i);
                mixed_byte ^= polynomsMultiplicationArray.at(layerBlock.at(i)).at(::lbox.at(i));
//                mixed_byte ^= this->gf256Multiplication(layerBlock.at(i), ::lbox.at(i));
            }

            *layerBlock.begin() = mixed_byte;
        }
    }

    void invLTransformation(LayerBlock &layerBlock)
    {
        for (auto &byte: layerBlock)
        {
            uint8_t mixed_byte = *layerBlock.begin();

            for (int i = 0; i < ::layerSize - 1; ++i)
            {
                layerBlock[i] = layerBlock.at(i + 1);
                mixed_byte ^= polynomsMultiplicationArray.at(layerBlock.at(i)).at(::lbox.at(i));
//                mixed_byte ^= this->gf256Multiplication(layerBlock.at(i), ::lbox.at(i));
            }

            *layerBlock.rbegin() = mixed_byte;
        }
    }

    // multiplication mod p(x) = x^8 + x^7 + x^6 + x + 1
    uint8_t gf256Multiplication(uint8_t first, uint8_t second)
    {
        uint8_t result = 0;

        while (second)
        {
            result ^= -(second & 1) & first;
            first = (first << 1) ^ (-((first & 0x80) >> 7) & 0xC3);
            second >>= 1;
        }

        return result;
    }
};

DozenPlus::DozenPlus() :
    d(new Impl)
{
}

DozenPlus::~DozenPlus() = default;

DataChain DozenPlus::ecbEncrypt(const DataChain &data, const DataBlock &key)
{
    DataChain encryptedData(data.size());
    DataBlock dataBlock;

    auto dataIterator = data.begin();
    auto endDataIterator = data.end();
    const auto beginDataBlockIterator = dataBlock.begin();
    auto encryptedDataIterator = encryptedData.begin();

    auto roundKeyArray = d->keyExpansion(key);

    // TODO(Yuranus): обработать кусок данных, который меньше 512 бит
    while (dataIterator < endDataIterator)
    {
        std::copy_n(dataIterator, ::blockSize, beginDataBlockIterator);
        dataIterator += ::blockSize;

        d->addRoundSubKeyToDataBlock(dataBlock, key);
        d->mixLayersWithSameType(dataBlock, Impl::LayerType::X, roundKeyArray.at(0));
        d->mixLayersWithSameType(dataBlock, Impl::LayerType::Y, roundKeyArray.at(1));
        d->mixLayersWithSameType(dataBlock, Impl::LayerType::Z, roundKeyArray.at(2));

        encryptedDataIterator = std::copy_n(beginDataBlockIterator, ::blockSize, encryptedDataIterator);
    }

    return encryptedData;
}

DataChain DozenPlus::ecbDecrypt(const DataChain &data, const DataBlock &key)
{
    DataChain decryptedData(data.size());
    DataBlock dataBlock;

    auto dataIterator = data.begin();
    auto endDataIterator = data.end();
    const auto beginDataBlockIterator = dataBlock.begin();
    auto decryptedDataIterator = decryptedData.begin();

    auto roundKeyArray = d->keyExpansion(key);

    // TODO(Yuranus): обработать кусок данных, который меньше 512 бит
    while (dataIterator < endDataIterator)
    {
        std::copy_n(dataIterator, ::blockSize, beginDataBlockIterator);
        dataIterator += ::blockSize;

        d->invMixLayersWithSameType(dataBlock, Impl::LayerType::Z, roundKeyArray.at(2));
        d->invMixLayersWithSameType(dataBlock, Impl::LayerType::Y, roundKeyArray.at(1));
        d->invMixLayersWithSameType(dataBlock, Impl::LayerType::X, roundKeyArray.at(0));
        d->addRoundSubKeyToDataBlock(dataBlock, key);

        decryptedDataIterator = std::copy_n(beginDataBlockIterator, ::blockSize, decryptedDataIterator);
    }

    return decryptedData;
}

DataChain DozenPlus::parallelEcbEncrypt(const DataChain &data, const DataBlock &key)
{
    DataChain encryptedData(data.size());
    DataBlock dataBlock;

    auto dataIterator = data.begin();
    auto encryptedDataIterator = encryptedData.begin();
    const auto beginDataBlockIterator = dataBlock.begin();

    auto roundKeyArray = d->keyExpansion(key);
    volatile bool breakCondition = false;

//    omp_set_dynamic(0);
//    omp_set_num_threads(4);
//    omp_set_nested(1);

#pragma omp parallel num_threads(4)
    {
        while (!breakCondition)
        {
#pragma omp single
            {
                std::copy_n(dataIterator, ::blockSize, beginDataBlockIterator);
                dataIterator += ::blockSize;
                d->addRoundSubKeyToDataBlock(dataBlock, key);
            }

            d->parallelMixLayersWithSameType(dataBlock, Impl::LayerType::X, roundKeyArray.at(0));
            d->parallelMixLayersWithSameType(dataBlock, Impl::LayerType::Y, roundKeyArray.at(1));
            d->parallelMixLayersWithSameType(dataBlock, Impl::LayerType::Z, roundKeyArray.at(2));

#pragma omp single
            {
                encryptedDataIterator = std::copy_n(beginDataBlockIterator, ::blockSize, encryptedDataIterator);
                breakCondition = encryptedDataIterator == encryptedData.end();
            }
        }
    }

    return encryptedData;
}

DataChain DozenPlus::parallelEcbDecrypt(const DataChain &data, const DataBlock &key)
{
    DataChain decryptedData(data.size());
    DataBlock dataBlock;

    auto dataIterator = data.begin();
    auto decryptedDataIterator = decryptedData.begin();
    const auto beginDataBlockIterator = dataBlock.begin();

    auto roundKeyArray = d->keyExpansion(key);
    volatile bool breakCondition = false;

#pragma omp parallel num_threads(4)
    {
        while (!breakCondition)
        {
#pragma omp single
            {
                std::copy_n(dataIterator, ::blockSize, beginDataBlockIterator);
                dataIterator += ::blockSize;
            }

            d->parallelInvMixLayersWithSameType(dataBlock, Impl::LayerType::Z, roundKeyArray.at(2));
            d->parallelInvMixLayersWithSameType(dataBlock, Impl::LayerType::Y, roundKeyArray.at(1));
            d->parallelInvMixLayersWithSameType(dataBlock, Impl::LayerType::X, roundKeyArray.at(0));

#pragma omp single
            {
                d->addRoundSubKeyToDataBlock(dataBlock, key);
                decryptedDataIterator = std::copy_n(beginDataBlockIterator, ::blockSize, decryptedDataIterator);
                breakCondition = decryptedDataIterator == decryptedData.end();
            }
        }
    }

    return decryptedData;
}
